
	let student = [];

	function addStudent(enrollees) {
		student.push(enrollees);
	}
	addStudent(`John`);
	console.log(`${student[0]} was added to the student's list`);

	addStudent(`Jane`);
	console.log(`${student[1]} was added to the student's list`);

	addStudent(`Joe`);
	console.log(`${student[2]} was added to the student's list`);
	

	function countStudents (){
		let totalStudents = student.length; 
		console.log(`There are a total of ${totalStudents} students enrolled`);
	}
	countStudents();

	function printStudents(){	
		student.sort();
		student.forEach(function (name){
			console.log(name);
		});
	}
	printStudents();


	function findStudent(keyword){

		let word = keyword.toLowerCase();


		let match = student.filter(function(name){
			// console.log(name);
			// console.log(name.includes(keyword));

			let lcName = name.toLowerCase();
			return lcName.includes(word)
		})

		// console.log(match)

		if(match.length === 1){
			console.log(`${match} is an enrollee`);
		}
		else if (match.length > 1){
			console.log(`${match} are enrollees`);
		}
		else{
			console.log(`${keyword} is not an enrollee`);
		}

	}
	findStudent(`ane`);
	
		
	// Stretch Goals - Add Section

	function addSection (section) {
		let sectionedStudents = student.map(function(name){
			return `${name} - section ${section}`;
		})
		console.log(sectionedStudents);
	}
	addSection(`A`);


	// Stretch Goals - Remove the Student
		
	function removedStudent(name){
		// console.log(name);

		let firstLetter = name.slice(0,1).toUpperCase();
		let remainingLetters = name.slice(1);
		let capName = firstLetter + remainingLetters;
		let studentIndex = student.indexOf(capName);
		
		if(studentIndex !== -1){
			student.splice(studentIndex, 1);
			console.log(`${capName} has been removed`)
		}

		
	}
	removedStudent(`John`);

	
	

